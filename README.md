# CI Task Request Guidelines


Below is the current process for new CI tasks:
1. CI to estimate the effort for new request
2. Client to discuss with Rey the request with details including criticality, etc and perform prioritization exercise
3. CI to wait for Rey and Wendell's approval before starting the task
4. If there are prioritization changes on the task list, relay to CI so that CI can work on the new priority task
5. Additional features not included in the initial agreed upon task list will need to go through the prioritization process again


## Pre-Assessment Checklist
1. A complete and detailed list of tasks
2. Translate the requirements in Use Cases, if possible
	```
    ex.
		As a <Type of User>, I want to be able to do the ff:
		1. <feature 1>
		2. <feature 2>

		As a recruiter, I want to be able to:
		1. See a complete list of job orders in a Google Sheet
		2. See stats based on the job order list:
			a. number of job orders per client
			b. number of applicants per job order per client per day
		3. Receive a summary of the stats daily via email
    ```
3. Prioritization of tasks if more than one.
4. Approximate date on when this project is needed.
5. Frequency of project status reports, if this is a big project.


## During Assessment
The Client should walk me through their current process and discuss the improvements/features they want to have.


##### Client should be ready to discuss the following and other related details
1. How your current system/process works
    - What tools do you currently use?
    - Who or what kind of users use the app/sheet/form?
    - When do you usually perform this task? (weekly, monthly, or only when a certain event happens)
    - Where do you get your data from? (i.e. PH roster sheet, or from Sprout, ...) and what is it's format? (xls, google sheet, pdf, or in a database, ...)
    - What problems do you usually encounter?  
  
  
2. What do you want the new system/process to be
    - Suggestions on improvement of current process.
    - Which steps should be improved/removed?
    - What new features do you want to add?
    - What data do you want to see? (i.e should display complete employee details even if the form only asks for employee ID )
    - What output should the new system give you? (in a google sheet, csv file)
    - Who should have access to this system?


## Project implementation
The start of the project will depend on the current projects being handled by CI. If this is a high priority task, please clear it first with Rey and Wendell so that schedules of other projects will be adjusted accordingly. __CI will start working on the project based on the agreed upon and provided list once the project is approved by Rey__.

The Client should be available for question/s regarding details of the task/s. Additional meetings might be needed for further clarifications.


## User Acceptance Testing
CI will give the project details (application link, google form/sheet link) once done. A demo will be given if needed. 

The Client will be given time to test the finished app. Kindly double check the app/pages before approving the final versions.

Immediate revisions before the app's initial run is highly discouraged. CI will not accommodate these specially major code changes. 

Future code updates and additional features not included in the initial agreed upon task list will need to go through the prioritization process again. 

